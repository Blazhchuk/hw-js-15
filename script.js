"use strict"


/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout? {

setTimeout - виконує функцію один раз після вказаного інтервалу.

setInterval - виконує функцію регулярно через кожен вказаний інтервал.

}

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval? {

Щоб припинити виконання потрібно використати такі функції - clearInterval && clearTimeout і в дужках вписати назву змінної в якій лежить відповідна функція.
    
}

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/


const button = document.getElementById('btn');
const div = document.getElementById('div');
const secondDiv = document.getElementById('div-two');

button.addEventListener('click', () => {
    setTimeout(() => {
        div.textContent = 'Операція успішно виконана!';
    }, 3000)

    let counter = 3;
    div.textContent = `Операцію буде виконано через ${counter} секунди...`;
    const count = setInterval(() => {
        counter--;
        if (counter > 0) {
            div.textContent = `Операцію буде виконано через ${counter} секунди...`;
        }
        else {
            clearInterval(count);
        }
    }, 1000);
});

let counter = 10;

const timer = setInterval(() => {
    secondDiv.textContent = `${counter}`;
    counter--;
    if (counter > 0) {
        secondDiv.textContent = `${counter}`;
    }
    else {
        secondDiv.textContent = 'Зворотній відлік завершено';
        clearInterval(timer);
    }
}, 1000);
